package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Write the numbers to be sorted separated by spaces");
        String input = scanner.nextLine();

        List<Integer> list = new ArrayList<Integer>();
        String[] numbers = input.split(" ");
        for (String number : numbers) {
            list.add(Integer.parseInt(number));
        }

        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(list);

        System.out.println(list);
    }
}




