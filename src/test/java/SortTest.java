
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.example.ArraySorter;




import static org.junit.jupiter.api.Assertions.assertEquals;
public class SortTest {
    @Test
    public void testSortEmptyArray() {
        List<Integer> array = new ArrayList<Integer>();
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Collections.emptyList(), array);
    }
    @Test
    public void testSortSingleElementArray() {
        List<Integer> array = new ArrayList<Integer>();
        array.add(5);
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Arrays.asList(5), array);
    }
    @Test
    public void testSortTwoElementArrayFirstLess() {
        List<Integer> array = new ArrayList<Integer>();
        array.add(3);
        array.add(5);
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Arrays.asList(3, 5), array);
    }
    @Test
    public void testSortTwoElementArrayFirstMore() {
        List<Integer> array = new ArrayList<Integer>();
        array.add(5);
        array.add(3);
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Arrays.asList(3, 5), array);
    }
    @Test
    public void testSortSortedArray() {
        List<Integer> array = new ArrayList<Integer>();
        array.add(-2);
        array.add(3);
        array.add(5);
        array.add(6);
        array.add(10);
        array.add(125);
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Arrays.asList(-2, 3, 5, 6, 10, 125), array);
    }
    @Test
    public void testSortReverseSortedArray() {
        List<Integer> array = new ArrayList<Integer>();
        array.add(125);
        array.add(10);
        array.add(6);
        array.add(5);
        array.add(3);
        array.add(-2);
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Arrays.asList(-2, 3, 5, 6, 10, 125), array);
    }
    @Test
    public void testSortSameValueArray() {
        List<Integer> array = new ArrayList<Integer>();
        array.add(5);
        array.add(5);
        array.add(5);
        array.add(5);
        array.add(5);
        ArraySorter arraySorter = new ArraySorter();
        arraySorter.sortArrayBubble(array);
        assertEquals(Arrays.asList(5, 5, 5, 5, 5), array);
    }

}
